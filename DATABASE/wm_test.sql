-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 04, 2019 at 04:14 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wm_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `house` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `zip` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `iban` varchar(100) NOT NULL,
  `paymentDataId` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `phone`, `email`, `location`, `house`, `street`, `zip`, `city`, `country`, `owner`, `iban`, `paymentDataId`, `created`) VALUES
(1, 'Sumit', 'Sharma', '+601153591613', 'sumitsharma42876@gmail.com', 'Home', '343', '3C 26 05, N Park Condominium, Jalan Batu Uban', '11700', 'Gelugor', 'Malaysia', 'Sumit Sharma', 'DE123456', '8dae91d68f056892d5da6d6d3909adeab9a5ac99aa6e00f7c6a2a5ed0291d3909a4c2bd6a90d1225791f07046e7e2048', '2019-11-04 04:12:21'),
(2, 'Paromita', 'Sengupta', '+601153592422', 'paro.moni@gmail.com', 'Home', '1122', 'WZ-79, Ground Floor,', '110064', 'NEW DELHI', 'India', 'Paromita Sengupta', 'DE123456789', '7aaad004bf6d0bcfbff66718223873e52d9008ebbba3deb3f9a3255424a74158e78d008266ddf72fc1001a724c6b57b1', '2019-11-04 04:13:39');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
