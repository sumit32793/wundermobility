1. Describe possible performance optimizations for your Code.

Answer: Yes, there is some, like on database we can do it by creating views on table, so we can fetch data fastly, and/or we can use the NoSQL(Not Only SQL), that is much better than MySQL, we can reduce the unnecessary data make the revisions of document, so we have the tace of everything.

_____________________________________________________________________________________

2. Which things could be done better, than you’ve done it?

Answer: I guess I can better develop the tab view. Right now it is developed and working perfectly, but I write too many lines of code for that, I like to reduce the line of code, so I think I can reduce it here..

_____________________________________________________________________________________

I structure my code to MVC pattern, becasue it's a small application which is interacting with database, view page and one controller, so I found MVC is good for my this small app.

______________________________________________________________________________________

GIT REPO : https://bitbucket.org/sumit32793/wundermobility/src/master/

DATABASE Folder is in root directory
APACHE Virtual Host is in root directory
HTML Template which I used is also in Root Directory
PHP Framwork Codeigniter version 3 is also in root directory
CSS/ Javascript in assets folder
_____________________________________________________________________________________

Files where I made change or create is here..

application/config/autoload.php 				# Autoload Helper and Libraries
application/config/config.php 					# Set Base URL
application/config/constants.php 				# Set Constant name
application/config/database.php 				# Set Database Configurations
application/config/routes.php 					# Set Routes for this application
application/controllers/Home.php 				# Home Controller have all logical code here
application/models/Common_model.php 			# Commor Model have common functions you can use these funcation anywere
application/views/home.php 						# View File
DATABASE/users.sql 								# MySQL Table
DATABASE/wm_test.sql 							# MySQL Database
HTML Template 									# Template HTML/CSS/Javascript
PHP Framwork Codeigniter 						# Codeigniter Framwork
.htaccess										# Changes to hise index.php from URL
README.txt										# CUrrent File
virtualhost.conf 								# Apache Host if you want to run on that
_____________________________________________________________________________________

Kind Regards,

Sumit Sharma
+60 115 359 1613
sumitsharma42876@gmail.com
http://sumitsharma.info/


