<?php
defined('BASEPATH') OR exit('No direct script access allowed'); // Define Basepath

class Home extends CI_Controller {								// Home Class Begins 

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index(){										// Home Function to load page and all operations
		if($this->input->post('fname')){							// Check if is there any POST Request
			$POSTDATA = $this->input->post();						// Hold all post data to a veriable	
			$data = $POSTDATA;										// Hold Post Vaiable to data variable which is used everywhere
			$data['created'] = date("Y/m/d h:i:sa");				// Get Current time of Our server
			$CustomerID = $this->common_model->insert_record(TBLUSER, $data); //Insert all Data to user table
            $fields = array('customerId' => $CustomerID, 'iban' => $data['iban'], 'owner' => $data['owner']); // Array of data which has to be sent to REST API
            $PostData = json_encode($fields);						// Convert fields array to json before sending
            $URL = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data"; // Rest API URL
            $ch = curl_init();										// cURL Initialisation
            curl_setopt($ch, CURLOPT_URL, $URL);					// URL Initialisation
            curl_setopt($ch, CURLOPT_POST, count($PostData));		// Count of Fields
            curl_setopt($ch, CURLOPT_POSTFIELDS, $PostData);		// POST Data
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);			// Return Transfer
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);			// Follow Location if redirection is there
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);				// Maximum Redirections
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);					// Set timeout
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");		// Method is Post
            // curl_setopt($ch, CURLOPT_PROXY, "http://proxy-us.intel.com"); // Working in Intel so I have to Proxy pass here
            // curl_setopt($ch, CURLOPT_PROXYPORT, "911");				// Intel Proxy code
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); // Header
            $result = curl_exec($ch);							// get response from API
            curl_close($ch);										// Close cURL
            $Result = json_decode($result, true);					// Json Decode response
            if(!empty($Result['paymentDataId'])){					// Check if we get Payment Data ID
            	$this->common_model->update_record($CustomerID, TBLUSER, $Result); // Update payment data id to users record
            	$this->session->set_userdata('message', "Dear ".$data['fname'].", You are successfully registered with us.<br />Please wait for a few hours our team will contact you.");								// Set session Message
            	$this->session->set_userdata('paymentDataId', $Result['paymentDataId']); // Set Data ID in Session
            } else {												// Else in case payment data id is not available
            	$this->session->set_userdata('message', "Something went wrong, Please try again to register."); // Set Error Message
            } 														// Close the Payment Data ID Check
            redirect("home/index/#services");						// Redirect to Payment Data ID Section
		}															// Close Post Data Check
		$this->load->view('home');									// Load Home Page inside view Folder
	}																// Close Function

	public function setsession(){									// Function For Setting Sessions
		if($this->input->post('fname')){							// Check if we have post data
			$POSTDATA = $this->input->post();						// Hold all post data to a variable
			foreach($POSTDATA as $key => $value){					// Foreach loop on Post data
				if(!empty($value)){									// Check if value is present
					$this->session->set_userdata($key, $value);		// Set POST field and value in session
				}													// Close value check
			}														// Close foreach loop
		}															// Close POST Data Check
	}																// Close Function

	public function new(){											// Function for new registration
		$this->session->sess_destroy();								// Delete session data for previuos registration
		redirect();													// redirect to Home
	}																// Close Function

}																	// Close Controller Class
