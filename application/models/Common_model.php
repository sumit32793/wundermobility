<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); // Define Basepath

class Common_model extends CI_Model{								// Common Model Class Begins
	
	public function __construct(){									// Constructor Initialisation for all functions
		parent::__construct();										// Load Parent Construtor
		$this->load->database();									// Load Database
    }																// Close Construtor Fucntion

	public function insert_record($tablename, $data){				// Insert Function Accept tablename and data to be inserted
        $this->db->insert($tablename, $data);						// Inserting Records
        $insert_id = $this->db->insert_id();						// Get the Last insert ID
        return $insert_id;											// Return Last insert ID for furthur operations
    }																// Close Insert Function

    public function update_record($id, $tablename, $data){			// Update Records Function Accept RowID, tablename and data
    	$this->db->set($data);										// Set data to be updated
		$this->db->where('id', $id);								// Where Clouse getting ID
		$this->db->update($tablename);								// Update query on table
    } 																// Close Update Function
  
}																	// Close Common Model Class

