<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Wunder Mobility :: Test</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/scrolling-nav.css'); ?>" rel="stylesheet">
<style>
* {
  box-sizing: border-box;
}

body {
  background-color: #f1f1f1;
}

#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  font-family: Raleway;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

select {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
select.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}
</style>
</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Wunder Mobility</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">Register</a>
          </li>
          <?php if(!empty($this->session->userdata('message'))){ ?>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Payment Data ID</a>
          </li>
        <?php } ?>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <header class="bg-primary text-white">
    <div class="container text-center">
      <h1>Welcome to Wunder Mobility</h1>
      <p class="lead">Register under the Wunder Mobility (Test Mode).</p>
    </div>
  </header>

  <section id="about">
    <div class="container">
      <div class="row">
       <form id="regForm" action="" method="POST">
        <h1>Register: <a style="float: right;" class="btn btn-primary" type="button" href="/home/new">New Registration</a></h1>
        <!-- One "tab" for each step in the form: -->
        <div class="tab">Personal Information:
          <p><input placeholder="First name..." oninput="this.className = ''" name="fname" value="<?php echo $this->session->userdata('fname'); ?>"></p>
          <p><input placeholder="Last name..." oninput="this.className = ''" name="lname" value="<?php echo $this->session->userdata('lname'); ?>"></p>
          <p><input placeholder="Phone..." oninput="this.className = ''" name="phone" value="<?php echo $this->session->userdata('phone'); ?>"></p>
          <p><input placeholder="E-mail..." oninput="this.className = ''" name="email" value="<?php echo $this->session->userdata('email'); ?>"></p>
        </div>
        <div class="tab">Address Information:
          <p><select name="location" oninput="this.className = ''">
            <option value="">Select Location Type...</option>
            <option value="Home" <?php if($this->session->userdata('location') == "Home"){ echo "selected"; } ?>>Home</option>
            <option value="Office" <?php if($this->session->userdata('location') == "Office"){ echo "selected"; } ?>>Office</option>
          </select></p>
          <p><input placeholder="House Number..." oninput="this.className = ''" name="house" value="<?php echo $this->session->userdata('house'); ?>"></p>
          <p><input placeholder="Street..." oninput="this.className = ''" name="street" value="<?php echo $this->session->userdata('street'); ?>"></p>
          <p><input placeholder="ZIP Code..." oninput="this.className = ''" name="zip" value="<?php echo $this->session->userdata('zip'); ?>"></p>
          <p><input placeholder="City..." oninput="this.className = ''" name="city" value="<?php echo $this->session->userdata('city'); ?>"></p>
          <p><input placeholder="Country..." oninput="this.className = ''" name="country" value="<?php echo $this->session->userdata('country'); ?>"></p>
        </div>
        <div class="tab">Payment Information:
          <p><input placeholder="Account Owner..." oninput="this.className = ''" name="owner" value="<?php echo $this->session->userdata('owner'); ?>"></p>
          <p><input placeholder="IBAN..." oninput="this.className = ''" name="iban" value="<?php echo $this->session->userdata('iban'); ?>"></p>
        </div>
        <div style="overflow:auto;">
          <div style="float:right;">
            <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
            <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
          </div>
        </div>
        <!-- Circles which indicates the steps of the form: -->
        <div style="text-align:center;margin-top:40px;">
          <span class="step"></span>
          <span class="step"></span>
          <span class="step"></span>
        </div>
      </form> 
      </div>
    </div>
  </section>

<?php if(!empty($this->session->userdata('message'))){ ?>
  <section id="services" class="bg-light">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2><?php echo $this->session->userdata('message'); ?></h2>
          <p class="lead">
            <?php
            if(!empty($this->session->userdata('paymentDataId'))){
              echo "Your Payment Data ID is : <strong>";
              echo $this->session->userdata('paymentDataId');
              echo "</strong><br /> Please copy your Payment Data ID, for furthur communication.";
            } ?>
            </p>
        </div>
      </div>
    </div>
  </section>
<?php } ?>

  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2>Contact us</h2>
          <p class="lead">Sumit Sharma</p>
          <p class="lead">+60 115 359 1613</p>
          <p class="lead">sumitsharma42876@gmail.com</p>
          <p class="lead">http://sumitsharma.info/</p>
          <p class="lead">https://www.wundermobility.com/</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Wunder Mobility 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="<?php echo base_url('assets/js/scrolling-nav.js'); ?>"></script>
<script>
  <?php
  if($this->session->userdata('location')){
    echo "var currentTab = 2;";
  } else if($this->session->userdata('fname')){
    echo "var currentTab = 1;";
  } else { echo "var currentTab = 0;"; }
  ?>
//var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  console.log(n);
  var values = $('form').serialize();
console.log(values);
 $.ajax({
        url: "/home/setsession",
        type: "post",
        data: values ,
        success: function (response) {
          console.log(response);
           // You will get response from your PHP page (what you echo or print)
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, z, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  z = x[currentTab].getElementsByTagName("select");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  for (i = 0; i < z.length; i++) {
    // If a field is empty...
    if (z[i].value == "") {
      // add an "invalid" class to the field:
      z[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>
</body>

</html>
